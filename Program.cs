﻿using System;
using System.IO.Pipes;
using System.Threading;

namespace StaticConstructor
{
     public class Program : StaticConstructor 
    {
        public Program() :base(10)
        {

        }   
        static void Main(string[] args)
        {                 
            Console.WriteLine("You are in Main Method");
            Program obj = new Program();
            StaticClassExample.staticAdd();
            var  f= StaticClassExample.name;
            Console.WriteLine(f);
            SealedExample obj4 = new SealedExample();
            obj4.AddOnSealedClass();
            StaticClassExample.AddOnSealedClass(obj4);
            string b = obj4.AddNum();
            var dd = b.WordCount();
            Console.WriteLine("Total number of words is :" + dd);
            string a = "hhg   nghg nn h hghhg";
             int totalWords = a.WordCount();
            Console.WriteLine("Total number of words is :" + totalWords);
        }
    } 
}
