﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace StaticConstructor
{
  public static class StaticClassExample
    {
       public static string name;
        static StaticClassExample()
        {
          name = "tset var";
        }

        public static void staticAdd()
        {
            Console.WriteLine("You are in staticAdd Method" + name);
        }

        public static void AddOnSealedClass(this SealedExample aa)
        {
            Console.WriteLine("You are in AddOnSealedClass Method");
        }
        public static int WordCount(this string str)
        {
            string[] userString = str.Split(new char[] { ' ', '.', '?' },
                                        StringSplitOptions.RemoveEmptyEntries);
            int wordCount = userString.Length;
            return wordCount;
        }
    }
}
