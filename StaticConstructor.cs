﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StaticConstructor 
{
    public class StaticConstructor 
    {
        static StaticConstructor()
        {
            Console.WriteLine("You are in Static Constructor");
        }
        public StaticConstructor(int a)
        {
            Console.WriteLine("You are in Static non-Constructor" +a);
        }
        public void Public()
        {
            Console.WriteLine("You are in Public method");
        }
        private void Private()
        {
            Console.WriteLine("You are in Private method");
        }
        protected void Protected()
        {
            Console.WriteLine("You are in  Protected method");
        }
        internal void Internal()
        {
            Console.WriteLine("You are in add Internal method");
        }
        protected internal void ProtectedInternal ()
        {
            Console.WriteLine("You are in protectedInternal method");
        }
        private protected void PrivateProtected ()
        {
            Console.WriteLine("You are in  PrivateProtected method");
        }
    }
}
